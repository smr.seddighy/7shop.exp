<?php
// dont user helpers , use Utility Instead !
function getActualUrl(){
	$actual_link = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://{$_SERVER['HTTP_HOST']}{$_SERVER['REQUEST_URI']}";
	return $actual_link;
}

function asset($type,$path){
    $whitelist = ['js','css','img'];
    if(!in_array($type,$whitelist))
        return "invalid_type";
    $type_url = BASE_URL."assets/$type/";
    $file_url = "{$type_url}{$path}";
    return $file_url;
}

function siteUrl($route){
    return BASE_URL.$route;
}