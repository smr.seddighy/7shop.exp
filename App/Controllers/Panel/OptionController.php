<?php
namespace App\Controllers\Panel;

use App\Core\Request;
use App\Models\Option;
use App\Repositories\OptionRepo;
use App\Services\Flash\FlashMessage;
use App\Services\Validator\Validator;
use App\Services\View\View;

class OptionController{

    public function index($request)
    {
        $optionRepo = new OptionRepo();
        $data = [
            'options' => $optionRepo->all()
        ];

        View::load('panel.options.index', $data, 'panel-admin');
    }

    public function add($request)
    {
        View::load('panel.options.add', [], 'panel-admin');
    }

    public function create(Request $request)
    {
        // validate data
        // filter data
        $vRes = $request->validate(array(
            'option_slug' => 'required|min_len,3|max_len,32',
            'option_title' => 'required|min_len,5',
            'option_value' => 'required',
            'option_cat' => 'required',
        ));
        if($vRes === true){
            $optionRepo = new OptionRepo();
            $optionRepo->create($request->params());
            FlashMessage::add("تنظیمات با موفقیت اضافه شد",FlashMessage::SUCCESS);
        }
        View::load('panel.options.add', [], 'panel-admin');
    }
}


// sql injection
// $inputU = "uuu' or '1'='1";
// Select count(*) from users where username='uuu' or '1'='1' and password="ppp";

// XSS
// salam khoobi <script>alert('haha')</script>

// Validation
// form : email, phone, url, age , msg

// CSRF