<?php

namespace App\Core;

use App\Services\Flash\FlashMessage;
use App\Services\Validator\Validator;

class Request
{
    public $method;
    public $uri;
    private $params;
    private $ip;
    private $agent;

    public function __construct()
    {
        $this->agent = $_SERVER['HTTP_USER_AGENT'];
        $this->ip = $_SERVER['REMOTE_ADDR'];
        $this->method = $_SERVER['REQUEST_METHOD'];
        $this->uri = $_SERVER['REQUEST_URI'];
        $this->params = $_REQUEST;
    }

    public function param($key)
    {
        return $this->params[$key];
    }

    public function params()
    {
        return $this->params;
    }

    public function __get($name)
    {
        if (array_key_exists($name, $this->params)) {
            return $this->param($name);
        }
    }

    public function keyExists($key)
    {
        return isset($this->params[$key]);
    }

    public function removeParam($key)
    {
        unset($this->params[$key]);
    }

    public function validate(array $validation_rules)
    {
        $vRes = Validator::is_valid($this->params, $validation_rules);
        if ($vRes === true) {
            return true;
        } else {
            foreach ($vRes as $errMsg) {
                FlashMessage::add($errMsg, FlashMessage::ERROR);
            }
        }
    }
    public function xss_clean(){
        $this->params = Validator::xss_clean($this->params);
    }
    public function sanitize($whitelist = NULL){
        $validator = Validator::get_instance();
        $this->params = $validator->sanitize($this->params,$whitelist);
    }
}